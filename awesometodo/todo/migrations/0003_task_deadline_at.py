# Generated by Django 2.1 on 2018-08-25 02:52

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0002_task_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='deadline_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
