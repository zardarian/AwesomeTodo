from django import forms
from .models import Task
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class TaskForm(forms.Form):
    name = forms.CharField(label="Task name", max_length=400)

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", "Add task"))


class TaskModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TaskModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", "Add task"))

    class Meta:
        model = Task
        fields = ["name", "description"]
